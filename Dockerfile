FROM maven:3.5.3-jdk-8
WORKDIR /app
COPY / /app
RUN mvn install -Dmaven.test.skip=true --batch-mode

FROM openjdk:8-jdk
COPY --from=0 /app/target/app.jar app.jar
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]
EXPOSE 80