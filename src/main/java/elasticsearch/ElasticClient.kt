package elasticsearch

import database.DatabaseCollection
import org.apache.http.entity.BasicHttpEntity
import org.apache.http.message.BasicHeader
import org.elasticsearch.ElasticsearchStatusException
import org.elasticsearch.action.admin.indices.create.CreateIndexRequest
import org.elasticsearch.action.bulk.BulkRequest
import org.elasticsearch.action.delete.DeleteRequest
import org.elasticsearch.action.index.IndexRequest
import org.elasticsearch.action.search.ClearScrollRequest
import org.elasticsearch.action.search.SearchRequest
import org.elasticsearch.action.search.SearchScrollRequest
import org.elasticsearch.client.RestHighLevelClient
import org.elasticsearch.common.unit.Fuzziness
import org.elasticsearch.index.query.QueryBuilders
import org.elasticsearch.search.builder.SearchSourceBuilder
import route.card.model.CardpackModel
import route.user.model.UserModel
import java.net.SocketException
import java.util.concurrent.TimeUnit
import java.util.Date
import java.util.Timer
import kotlin.collections.HashMap
import kotlin.concurrent.schedule
import org.elasticsearch.common.unit.TimeValue
import org.elasticsearch.index.query.QueryBuilders.matchAllQuery
import org.elasticsearch.search.Scroll
import kotlin.collections.ArrayList

class ElasticClient(private val elasticClient: RestHighLevelClient, private val database: DatabaseCollection, timeoutDuration: Int, autoReindex: Boolean = true) : ElasticInterface {
    companion object {
        const val userIndex = "user"
        const val userAutocompleteIndex = "userautocomplete"
        const val cardpackIndex = "cardpack"
        const val cardpackAutocompleteIndex = "cardpackautocomplete"
        const val maxBatchSize = 5000
    }
    private val reindexerTimer = Timer()

    init {
        createIndices(timeoutDuration)

        if (autoReindex) {
            val reindexDelayMillis = TimeUnit.MINUTES.toMillis(1)
            reindexerTimer.schedule(0, reindexDelayMillis) {
                reindexAllData()
            }
        }
    }

    override fun indexUser(user: UserModel) {
        elasticClient.index(IndexRequest(userIndex).source("name", user.name).id(user.id).type(userIndex))
        elasticClient.index(IndexRequest(userAutocompleteIndex).source("name", user.name).id(user.id).type(userAutocompleteIndex))
    }

    override fun indexUsers(users: List<UserModel>) {
        if (users.isNotEmpty()) {
            val bulkRequest = BulkRequest()
            users.forEach {
                bulkRequest.add(IndexRequest(userIndex).source("name", it.name).id(it.id).type(userIndex))
                bulkRequest.add(IndexRequest(userAutocompleteIndex).source("name", it.name).id(it.id).type(userAutocompleteIndex))
            }
            elasticClient.bulk(bulkRequest)
        }
    }

    override fun indexCardpack(cardpack: CardpackModel) {
        elasticClient.index(IndexRequest(cardpackIndex).source("name", cardpack.name).id(cardpack.id).type(cardpackIndex))
        elasticClient.index(IndexRequest(cardpackAutocompleteIndex).source("name", cardpack.name).id(cardpack.id).type(cardpackAutocompleteIndex))
    }

    override fun indexCardpacks(cardpacks: List<CardpackModel>) {
        if (cardpacks.isNotEmpty()) {
            val bulkRequest = BulkRequest()
            cardpacks.forEach {
                bulkRequest.add(IndexRequest(cardpackIndex).source("name", it.name).id(it.id).type(cardpackIndex))
                bulkRequest.add(IndexRequest(cardpackAutocompleteIndex).source("name", it.name).id(it.id).type(cardpackAutocompleteIndex))
            }
            elasticClient.bulk(bulkRequest)
        }
    }

    override fun unindexCardpack(cardpackId: String) {
        elasticClient.delete(DeleteRequest(cardpackIndex).type(cardpackIndex).id(cardpackId))
    }

    override fun unindexCardpacks(cardpackIds: List<String>) {
        if (cardpackIds.isNotEmpty()) {
            val bulkRequest = BulkRequest()
            cardpackIds.forEach {
                bulkRequest.add(DeleteRequest(cardpackIndex).type(cardpackIndex).id(it))
            }
            elasticClient.bulk(bulkRequest)
        }
    }

    override fun searchUsers(query: String): List<UserModel> {
        val searchSourceBuilder = SearchSourceBuilder().query(QueryBuilders.multiMatchQuery(query, "name").fuzziness(Fuzziness.AUTO))
        val searchRequest = SearchRequest(userIndex).source(searchSourceBuilder)
        val res = elasticClient.search(searchRequest)
        val userIds = res.hits.hits.map { hit -> hit.id }
        return database.getUsers(userIds)
    }

    override fun searchCardpacks(query: String): List<CardpackModel> {
        val searchSourceBuilder = SearchSourceBuilder().query(QueryBuilders.multiMatchQuery(query, "name").fuzziness(Fuzziness.AUTO))
        val searchRequest = SearchRequest(cardpackIndex).source(searchSourceBuilder)
        val res = elasticClient.search(searchRequest)
        val cardpackIds = res.hits.hits.map { hit -> hit.id }
        return cardpackIds.map { id -> database.getCardpack(id) } // TODO - Optimize this line
    }

    override fun autoCompleteUserSearch(query: String, max: Int): List<String> {
        val searchSourceBuilder = SearchSourceBuilder().query(QueryBuilders.matchQuery("name", query)).size(max)
        val searchRequest = SearchRequest(userAutocompleteIndex).source(searchSourceBuilder)
        val res = elasticClient.search(searchRequest)
        val userIds = res.hits.hits.map { hit -> hit.id }
        return database.getUsers(userIds).map { user -> user.name }
    }

    override fun autoCompleteCardpackSearch(query: String, max: Int): List<String> {
        val searchSourceBuilder = SearchSourceBuilder().query(QueryBuilders.matchQuery("name", query)).size(max)
        val searchRequest = SearchRequest(cardpackAutocompleteIndex).source(searchSourceBuilder)
        val res = elasticClient.search(searchRequest)
        val cardpackIds = res.hits.hits.map { hit -> hit.id }
        return cardpackIds.map { id -> database.getCardpack(id).name } // TODO - Make this more efficient
    }

    override fun reindexAllData() {
        reindexAllUsers()
        unindexDeletedCardpacks()
        reindexAllCardpacks()
    }

    private fun reindexAllUsers() {
        val userQueue: MutableList<UserModel> = ArrayList() // List of users stored in memory so that we can perform batch indexes with elasticsearch
        database.forEachUser {
            userQueue.add(it)
            if (userQueue.size == maxBatchSize) { // This is where you can change the batch size
                indexUsers(userQueue)
                userQueue.clear()
            }
        }
        indexUsers(userQueue)
    }

    private fun reindexAllCardpacks() {
        val cardpackQueue: MutableList<CardpackModel> = ArrayList() // List of cardpacks stored in memory so that we can perform batch indexes with elasticsearch
        database.forEachCardpack {
            cardpackQueue.add(it)
            if (cardpackQueue.size == maxBatchSize) { // This is where you can change the batch size
                indexCardpacks(cardpackQueue)
                cardpackQueue.clear()
            }
        }
        indexCardpacks(cardpackQueue)
    }

    private fun unindexDeletedCardpacks() {
        val cardpackIdBuffer: MutableList<String> = ArrayList()
        val deletedCardpackIds: MutableList<String> = ArrayList()

        forEachCardpackId {
            cardpackIdBuffer.add(it)
            if (cardpackIdBuffer.size == maxBatchSize) {
                deletedCardpackIds.addAll(database.filterUnusedCardpackIds(cardpackIdBuffer))
                cardpackIdBuffer.clear()
                if (deletedCardpackIds.size == maxBatchSize) {
                    unindexCardpacks(deletedCardpackIds)
                    deletedCardpackIds.clear()
                }
            }
        }
        unindexCardpacks(deletedCardpackIds)
        deletedCardpackIds.clear()

        // This section should be redundant, but just in case.
        forEachAutocompleteCardpackId {
            cardpackIdBuffer.add(it)
            if (cardpackIdBuffer.size == maxBatchSize) {
                deletedCardpackIds.addAll(database.filterUnusedCardpackIds(cardpackIdBuffer))
                cardpackIdBuffer.clear()
                if (deletedCardpackIds.size == maxBatchSize) {
                    unindexCardpacks(deletedCardpackIds)
                    deletedCardpackIds.clear()
                }
            }
        }
        unindexCardpacks(deletedCardpackIds)
        deletedCardpackIds.clear()
    }

    private fun forEachCardpackId(fn: (String) -> Unit): Boolean {
        return forEachIdOfIndex(fn, cardpackIndex)
    }

    private fun forEachAutocompleteCardpackId(fn: (String) -> Unit): Boolean {
        return forEachIdOfIndex(fn, cardpackAutocompleteIndex)
    }

    // Calls fn for each document in a particular index, passing the document's ID in as an argument
    private fun forEachIdOfIndex(fn: (String) -> Unit, index: String): Boolean {
        val scroll = Scroll(TimeValue.timeValueMinutes(1L))
        val searchRequest = SearchRequest(index)
        searchRequest.scroll(scroll)
        val searchSourceBuilder = SearchSourceBuilder()
        searchSourceBuilder.query(matchAllQuery())
        searchSourceBuilder.size(maxBatchSize)
        searchRequest.source(searchSourceBuilder)

        var searchResponse = elasticClient.search(searchRequest)
        var scrollId = searchResponse.scrollId
        var searchHits = searchResponse.hits.hits

        while (searchHits != null && searchHits.isNotEmpty()) {
            val scrollRequest = SearchScrollRequest(scrollId)
            scrollRequest.scroll(scroll)
            searchResponse = elasticClient.searchScroll(scrollRequest)
            scrollId = searchResponse.scrollId
            searchHits = searchResponse.hits.hits

            // Process searchHits
            searchHits.forEach { fn(it.id) }
        }

        val clearScrollRequest = ClearScrollRequest()
        clearScrollRequest.addScrollId(scrollId)
        val clearScrollResponse = elasticClient.clearScroll(clearScrollRequest)
        return clearScrollResponse.isSucceeded
    }

    private fun createIndices(timeoutDuration: Int) {
        waitForConnection(timeoutDuration)
        try {
            createAutocompleteIndex(userAutocompleteIndex)
        } catch (e: Exception) { }
        try {
            createAutocompleteIndex(cardpackAutocompleteIndex)
        } catch (e: Exception) { }
        try {
            elasticClient.indices().create(CreateIndexRequest(userIndex))
        } catch (e: ElasticsearchStatusException) { println(e.message) }
        try {
            elasticClient.indices().create(CreateIndexRequest(cardpackIndex))
        } catch (e: Exception) { }
    }

    private fun waitForConnection(timeoutDuration: Int) {
        if (timeoutDuration <= 0) {
            throw Exception("Timeout duration must be positive")
        }
        val startTime = Date()
        var lastPinged = Date(0)
        while (true) {
            if (Date().time - lastPinged.time > 1000) {
                lastPinged = Date()
                try {
                    if (elasticClient.ping()) {
                        break
                    }
                } catch (e: Exception) { }
            }
            if (Date().time - startTime.time > timeoutDuration) {
                throw SocketException("Could not connect to Elasticsearch")
            }
            Thread.yield()
        }
    }

    private fun createAutocompleteIndex(index: String) {
        // TODO - Check if index exists and add error handling
        var httpEntity = BasicHttpEntity()
        httpEntity.content = (
                        "{" +
                        "    \"settings\": {" +
                        "        \"analysis\": {" +
                        "            \"filter\": {" +
                        "                \"autocomplete_filter\": { " +
                        "                    \"type\":     \"edge_ngram\"," +
                        "                    \"min_gram\": 1," +
                        "                    \"max_gram\": 20" +
                        "                }" +
                        "            }," +
                        "            \"analyzer\": {" +
                        "                \"autocomplete\": {" +
                        "                    \"type\":      \"custom\"," +
                        "                    \"tokenizer\": \"standard\"," +
                        "                    \"filter\": [" +
                        "                        \"lowercase\"," +
                        "                        \"autocomplete_filter\" " +
                        "                    ]" +
                        "                }" +
                        "            }" +
                        "        }" +
                        "    }," +
                        "    \"mappings\": {" +
                        "        \"$index\": {" +
                        "            \"properties\": {" +
                        "                \"name\": {" +
                        "                    \"type\": \"text\"," +
                        "                    \"analyzer\": \"autocomplete\", " +
                        "                    \"search_analyzer\": \"standard\" " +
                        "                }" +
                        "            }" +
                        "        }" +
                        "    }" +
                        "}"
                ).byteInputStream()
        httpEntity.contentType = BasicHeader("Content-Type", "application/json")
        elasticClient.lowLevelClient.performRequest("PUT", "/$index", HashMap<String, String>(), httpEntity)
    }
}