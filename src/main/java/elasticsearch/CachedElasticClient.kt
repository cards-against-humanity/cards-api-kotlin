package elasticsearch

import com.google.common.cache.Cache
import com.google.common.cache.CacheBuilder
import route.card.model.CardpackModel
import route.user.model.UserModel
import java.time.Duration

class CachedElasticClient(private val parent: ElasticInterface) : ElasticInterface {
    private companion object {
        fun getCacheBuilder(
            maxSize: Long = 10000,
            expireDuration: Duration = Duration.ofMinutes(10)
        ): CacheBuilder<Any, Any> {
            return CacheBuilder.newBuilder()
                    .maximumSize(maxSize)
                    .expireAfterWrite(expireDuration)
        }

        /**
         * Checks if item matching the query is already in the cache and
         * adds it to the cache if it does not exist, otherwise returning
         * the cached version without calling the potentially I/O-bound
         * getItem lambda
         */
        fun <Q, V> updateCacheAndGetItem(query: Q, cache: Cache<Q, V>, getItem: (Q) -> V): V {
            val cachedItems = cache.getIfPresent(query)

            return if (cachedItems != null) {
                cachedItems
            } else {
                val result = getItem(query)
                cache.put(query, result)
                result
            }
        }
    }

    private val userCache: Cache<String, List<UserModel>> = getCacheBuilder().build()
    private val cardpackCache: Cache<String, List<CardpackModel>> = getCacheBuilder().build()
    private val userAutocompleteCache: Cache<String, List<String>> = getCacheBuilder().build()
    private val cardpackAutocompleteCache: Cache<String, List<String>> = getCacheBuilder().build()

    override fun indexUser(user: UserModel) {
        parent.indexUser(user)
    }

    override fun indexUsers(users: List<UserModel>) {
        parent.indexUsers(users)
    }

    override fun indexCardpack(cardpack: CardpackModel) {
        parent.indexCardpack(cardpack)
    }

    override fun indexCardpacks(cardpacks: List<CardpackModel>) {
        parent.indexCardpacks(cardpacks)
    }

    override fun unindexCardpack(cardpackId: String) {
        parent.unindexCardpack(cardpackId)
    }

    override fun unindexCardpacks(cardpackIds: List<String>) {
        parent.unindexCardpacks(cardpackIds)
    }

    override fun searchUsers(query: String): List<UserModel> {
        return updateCacheAndGetItem(query, userCache, parent::searchUsers)
    }

    override fun searchCardpacks(query: String): List<CardpackModel> {
        return updateCacheAndGetItem(query, cardpackCache, parent::searchCardpacks)
    }

    override fun autoCompleteUserSearch(query: String, max: Int): List<String> {
        return updateCacheAndGetItem(query, userAutocompleteCache) { q -> parent.autoCompleteUserSearch(q, max) }
    }

    override fun autoCompleteCardpackSearch(query: String, max: Int): List<String> {
        return updateCacheAndGetItem(query, cardpackAutocompleteCache) { q -> parent.autoCompleteCardpackSearch(q, max) }
    }

    override fun reindexAllData() {
        parent.reindexAllData()
    }
}