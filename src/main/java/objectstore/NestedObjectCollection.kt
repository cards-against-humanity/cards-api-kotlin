package objectstore

import com.google.common.net.MediaType

class NestedObjectCollection(private val collection: ObjectCollection, private val directory: String) : ObjectCollection {
    init {
        if (!ObjectCollection.isValidKey(directory)) {
            throw Exception("Invalid object key")
        }
    }

    override fun putObject(key: String, data: ByteArray, type: MediaType) {
        if (!ObjectCollection.isValidKey(key)) {
            throw Exception("Invalid object key")
        }
        collection.putObject("$directory/$key", data, type)
    }

    override fun getObject(key: String): ObjectCollection.ObjectWrapper {
        if (!ObjectCollection.isValidKey(key)) {
            throw Exception("Invalid object key")
        }
        return collection.getObject("$directory/$key")
    }

    override fun deleteObject(key: String) {
        if (!ObjectCollection.isValidKey(key)) {
            throw Exception("Invalid object key")
        }
        collection.deleteObject("$directory/$key")
    }
}