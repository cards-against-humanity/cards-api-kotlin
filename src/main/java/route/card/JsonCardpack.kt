package route.card

import com.fasterxml.jackson.annotation.JsonProperty

data class JsonCardpack(@JsonProperty("name") val name: String)