package server

import com.mongodb.MongoClient
import com.mongodb.MongoClientURI
import com.mongodb.client.MongoDatabase
import config.SwaggerConfig
import database.mongomodel.MongoDatabaseCollection
import elasticsearch.CachedElasticClient
import elasticsearch.ElasticClient
import elasticsearch.ElasticSearchableDatabaseCollection
import elasticsearch.SearchableDatabaseCollection
import objectstore.ImageObjectCollection
import objectstore.NestedObjectCollection
import objectstore.ObjectCollection
import objectstore.S3ObjectCollection
import org.apache.http.HttpHost
import org.elasticsearch.client.RestClient
import org.elasticsearch.client.RestHighLevelClient
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.autoconfigure.mongo.MongoAutoConfiguration
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.web.servlet.config.annotation.InterceptorRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter
import route.AuthInterceptor
import route.card.CardController
import route.search.SearchController
import route.user.UserController
import org.springframework.web.servlet.config.annotation.CorsRegistry
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer
import org.apache.http.auth.UsernamePasswordCredentials
import org.apache.http.auth.AuthScope
import org.apache.http.impl.client.BasicCredentialsProvider

@SpringBootApplication(exclude = [MongoAutoConfiguration::class])
@ComponentScan(basePackageClasses = [UserController::class, CardController::class, SearchController::class, SwaggerConfig::class])
open class Main : WebMvcConfigurerAdapter() {
    companion object {
        @JvmStatic
        fun main(args: Array<String>) {
            SpringApplication.run(Main::class.java, *args)
        }
    }

    override fun addInterceptors(registry: InterceptorRegistry) {
        registry.addInterceptor(AuthInterceptor())
    }

    @Bean
    open fun getDatabase(args: Args): SearchableDatabaseCollection {
        val db = MongoDatabaseCollection(
                getMongoDatabase(
                        args.mongoHost,
                        args.mongoDatabase,
                        args.mongoUsername,
                        args.mongoPassword
                )
        )

        val elasticClient = CachedElasticClient(ElasticClient(
                getElasticsearchHighLevelClient(
                        args.elasticsearchHost,
                        args.elasticsearchPort,
                        args.elasticsearchConnectionScheme,
                        args.elasticsearchUsername,
                        args.elasticsearchPassword
                ),
                db,
                30000
        ))

        return ElasticSearchableDatabaseCollection(db, elasticClient)
    }

    @Bean(name = ["mainObjectCollection"])
    open fun getObjectCollection(args: Args): ObjectCollection {
        return S3ObjectCollection(args.s3Bucket, args.s3AccessKey, args.s3PrivateKey, args.s3Endpoint, args.s3Region)
    }

    @Bean(name = ["profilePicturesCollection"])
    open fun getProfilePicturesCollection(args: Args, @Qualifier("mainObjectCollection") objectCollection: ObjectCollection): ImageObjectCollection {
        return ImageObjectCollection(NestedObjectCollection(objectCollection, "profilePictures"))
    }

    private fun getMongoDatabase(hostUri: String, databaseName: String, username: String?, password: String?): MongoDatabase {
        if ((username == null) != (password == null)) {
            throw IllegalArgumentException("Cannot provide mongodb username OR password - must provide both or neither")
        }

        return if (username != null && password != null) {
            try {
                MongoClient(MongoClientURI("mongodb+srv://$username:$password@$hostUri/test?retryWrites=true"))
            } catch (e: IllegalArgumentException) {
                MongoClient(MongoClientURI("mongodb://$username:$password@$hostUri/test?retryWrites=true"))
            }
        } else {
            try {
                MongoClient(MongoClientURI("mongodb+srv://@$hostUri/test?retryWrites=true"))
            } catch (e: IllegalArgumentException) {
                MongoClient(MongoClientURI("mongodb://@$hostUri/test?retryWrites=true"))
            }
        }.getDatabase(databaseName)
    }

    private fun getElasticsearchHighLevelClient(host: String, port: Int, connectionScheme: String, username: String?, password: String?): RestHighLevelClient {
        if ((username == null) != (password == null)) {
            throw IllegalArgumentException("Cannot provide elasticsearch username OR password - must provide both or neither")
        }

        val clientBuilder = RestClient.builder(HttpHost(host, port, connectionScheme))

        if (username != null && password != null) {
            val credentialsProvider = BasicCredentialsProvider()
            credentialsProvider.setCredentials(AuthScope.ANY, UsernamePasswordCredentials(username, password))

            clientBuilder.setHttpClientConfigCallback {
                asyncClientBuilder -> asyncClientBuilder.setDefaultCredentialsProvider(credentialsProvider)
            }
        }

        return RestHighLevelClient(clientBuilder)
    }

    @Bean
    open fun corsConfigurer(args: Args): WebMvcConfigurer {
        return object : WebMvcConfigurerAdapter() {
            override fun addCorsMappings(registry: CorsRegistry) {
                registry.addMapping("/**").allowedMethods("GET", "POST", "PUT", "DELETE").allowedOrigins(args.allowedCorsOrigin).allowedHeaders("*")
            }
        }
    }

    @Bean
    open fun getArgs(): Args {
        return Args()
    }
}