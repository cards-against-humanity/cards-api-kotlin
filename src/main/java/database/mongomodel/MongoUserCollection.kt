package database.mongomodel

import com.mongodb.client.MongoCollection
import com.mongodb.client.model.IndexOptions
import org.bson.Document
import org.bson.types.ObjectId
import route.user.model.UserCollection
import route.user.model.UserModel

class MongoUserCollection(private val mongoCollection: MongoCollection<Document>) : UserCollection {
    private companion object {
        fun convertDocumentToMongoUserModel(doc: Document): MongoUserModel {
            val id = doc["_id"] as ObjectId
            return MongoUserModel(id.toHexString(), doc["name"] as String)
        }
    }

    init {
        mongoCollection.createIndex(Document("oAuthProvider", 1).append("oAuthId", 1), IndexOptions().unique(true))
    }

    override fun getUser(id: String): UserModel {
        val doc: Document
        try {
            doc = mongoCollection.find(Document("_id", ObjectId(id))).first()
        } catch (e: Exception) {
            throw Exception("User does not exist with id: $id")
        }
        return convertDocumentToMongoUserModel(doc)
    }

    override fun getUsers(ids: List<String>): List<UserModel> {
        return ids.map { id -> getUser(id) }
    }

    override fun getUser(oAuthId: String, oAuthProvider: String): UserModel {
        val doc: Document = try {
            mongoCollection.find(Document("oAuthId", oAuthId).append("oAuthProvider", oAuthProvider)).first()
        } catch (e: Exception) {
            throw Exception("User does not exist with oAuthId of $oAuthId and oAuthProvider of $oAuthProvider")
        }
        return convertDocumentToMongoUserModel(doc)
    }

    override fun createUser(name: String, oAuthId: String, oAuthProvider: String): UserModel {
        val id = ObjectId()
        val doc = Document()
                .append("_id", id)
                .append("oAuthId", oAuthId)
                .append("oAuthProvider", oAuthProvider)
                .append("name", name)
        try {
            mongoCollection.insertOne(doc)
        } catch (e: Exception) {
            throw Exception("User already exists with that oAuth ID and provider")
        }
        return getUser(id.toHexString())
    }

    override fun setUserName(id: String, name: String): UserModel {
        val objectId = try {
            ObjectId(id)
        } catch (e: Exception) {
            throw Exception("User does not exist with id: $id")
        }
        if (name.isEmpty()) {
            throw Exception("Cannot set name to an empty string")
        }
        val response = mongoCollection.updateOne(Document("_id", objectId), Document("\$set", Document("name", name)))
        if (response.matchedCount == 0L) {
            throw Exception("User does not exist with id: $id")
        }
        return getUser(id)
    }

    override fun forEachUser(fn: (UserModel) -> Unit) {
        mongoCollection.find().forEach { fn(convertDocumentToMongoUserModel(it)) }
    }

    private class MongoUserModel(override val id: String, override var name: String) : UserModel
}