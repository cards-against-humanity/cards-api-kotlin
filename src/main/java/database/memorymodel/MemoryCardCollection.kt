package database.memorymodel

import route.card.JsonBlackCard
import route.card.JsonWhiteCard
import route.card.model.BlackCardModel
import route.card.model.CardCollection
import route.card.model.CardpackModel
import route.card.model.WhiteCardModel
import route.user.model.UserCollection
import route.user.model.UserModel
import java.util.Date
import kotlin.collections.HashSet

class MemoryCardCollection(private val userCollection: UserCollection) : CardCollection {
    private var nextId = 0
    private val cardpacks: MutableMap<String, MutableList<MemoryCardpackModel>> = HashMap()

    override fun createCardpack(name: String, userId: String): CardpackModel {
        val user = userCollection.getUser(userId)
        if (cardpacks[userId] == null) {
            cardpacks[userId] = ArrayList()
        }
        val cardpack = MemoryCardpackModel(this.nextId++.toString(), name, user)
        cardpacks[userId]!!.add(cardpack)
        return cardpack
    }

    override fun deleteCardpack(cardpackId: String) {
        if (cardpacks[cardpackId] == null) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }
        cardpacks.remove(cardpackId)
    }

    override fun getCardpack(cardpackId: String): CardpackModel {
        return _getCardpack(cardpackId)
    }

    override fun getCardpacks(userId: String): List<CardpackModel> {
        userCollection.getUser(userId)
        return cardpacks[userId] ?: ArrayList()
    }

    override fun createWhiteCard(cardData: JsonWhiteCard, cardpackId: String): WhiteCardModel {
        val cardpack = this.getCardpack(cardpackId) as MemoryCardpackModel
        val card = MemoryWhiteCardModel(this.nextId++.toString(), cardData.text, cardpackId)
        cardpack.whiteCards.add(card)
        return card
    }

    override fun createWhiteCards(cardDataList: List<JsonWhiteCard>, cardpackId: String): List<WhiteCardModel> {
        // TODO - Test for atomicity
        return cardDataList.map { cardData -> this.createWhiteCard(cardData, cardpackId) }
    }

    override fun createBlackCard(cardData: JsonBlackCard, cardpackId: String): BlackCardModel {
        val cardpack = this.getCardpack(cardpackId) as MemoryCardpackModel
        val card = MemoryBlackCardModel(this.nextId++.toString(), cardData.text, cardData.answerFields, cardpackId)
        cardpack.blackCards.add(card)
        return card
    }

    override fun createBlackCards(cardDataList: List<JsonBlackCard>, cardpackId: String): List<BlackCardModel> {
        // TODO - Test for atomicity
        return cardDataList.map { cardData -> this.createBlackCard(cardData, cardpackId) }
    }

    override fun deleteWhiteCard(cardId: String, cardpackId: String) {
        if (!cardpacks.containsKey(cardpackId)) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }
        for (entry in cardpacks) {
            val cardpackList = entry.value
            for (cardpack in cardpackList) {
                for (card in cardpack.whiteCards) {
                    if (card.id == cardId) {
                        cardpack.whiteCards.remove(card)
                        return
                    }
                }
            }
        }
        throw Exception("Card does not exist with id: $cardId")
    }

    override fun deleteWhiteCards(ids: List<String>, cardpackId: String) {
        val whiteCards = _getCardpack(cardpackId).whiteCards
        ids.forEach { id ->
            if (!whiteCards.map { it.id }.contains(id)) {
                throw Exception("One or more card ids is invalid")
            }
        }
        ids.forEach { id -> this.deleteWhiteCard(id, cardpackId) }
    }

    override fun deleteBlackCard(cardId: String, cardpackId: String) {
        if (!cardpacks.containsKey(cardpackId)) {
            throw Exception("Cardpack does not exist with id: $cardpackId")
        }
        for (entry in cardpacks) {
            val cardpackList = entry.value
            for (cardpack in cardpackList) {
                for (card in cardpack.blackCards) {
                    if (card.id == cardId) {
                        cardpack.blackCards.remove(card)
                        return
                    }
                }
            }
        }
        throw Exception("Card does not exist with id: $cardId")
    }

    override fun deleteBlackCards(ids: List<String>, cardpackId: String) {
        val blackCards = _getCardpack(cardpackId).blackCards
        ids.forEach { id ->
            if (!blackCards.map { it.id }.contains(id)) {
                throw Exception("One or more card ids is invalid")
            }
        }
        ids.forEach { id -> this.deleteBlackCard(id, cardpackId) }
    }

    override fun favoriteCardpack(userId: String, cardpackId: String) {
        val cardpack = _getCardpack(cardpackId)
        if (cardpack.owner.id == userId) {
            throw Exception("Cannot favorite your own cardpack")
        }
        if (cardpack.favoriterUserIds.contains(userId)) {
            throw Exception("Cardpack is already in your favorites")
        }
        userCollection.getUser(userId)
        cardpack.favoriterUserIds.add(userId)
    }

    override fun unfavoriteCardpack(userId: String, cardpackId: String) {
        val cardpack = _getCardpack(cardpackId)
        if (!cardpack.favoriterUserIds.contains(userId)) {
            throw Exception("Cardpack is not in your favorites")
        }
        cardpack.favoriterUserIds.remove(userId)
    }

    override fun getFavoritedCardpacks(userId: String): List<CardpackModel> {
        val favoritedCardpacks: MutableList<CardpackModel> = ArrayList()
        cardpacks.values.forEach { it.forEach {
            if (it.favoriterUserIds.contains(userId)) {
                favoritedCardpacks.add(it)
            }
        } }
        return favoritedCardpacks
    }

    override fun cardpackIsFavoritedByUser(userId: String, cardpackId: String): Boolean {
        val cardpack = cardpacks.values.flatten().find { it.id == cardpackId } ?: throw Exception("Cardpack does not exist with id: $cardpackId")
        return cardpack.favoriterUserIds.contains(userId)
    }

    override fun forEachCardpack(fn: (CardpackModel) -> Unit) {
        cardpacks.values.flatten().forEach { fn(it) }
    }

    override fun filterUnusedCardpackIds(ids: List<String>): List<String> {
        val unusedCardpackIds: MutableList<String> = ArrayList()
        val cardpackIds = cardpacks.values.flatten().map { it.id }
        ids.forEach {
            if (!cardpackIds.contains(it)) {
                unusedCardpackIds.add(it)
            }
        }
        return unusedCardpackIds
    }

    private fun _getCardpack(cardpackId: String): MemoryCardpackModel {
        for (entry in cardpacks) {
            val cardpackList = entry.value
            for (cardpack in cardpackList) {
                if (cardpack.id == cardpackId) {
                    return cardpack
                }
            }
        }
        throw Exception("Cardpack does not exist with id: $cardpackId")
    }

    private class MemoryCardpackModel(override val id: String, override var name: String, override val owner: UserModel) : CardpackModel {
        override val whiteCards: MutableList<WhiteCardModel> = ArrayList()
        override val blackCards: MutableList<BlackCardModel> = ArrayList()
        override val createdAt = Date()
        override val likeCount: Long
            get() = favoriterUserIds.size.toLong()
        val favoriterUserIds: MutableSet<String> = HashSet()

        override fun setName(name: String): CardpackModel {
            this.name = name
            return this
        }
    }

    private class MemoryWhiteCardModel(override val id: String, override var text: String, override val cardpackId: String) : WhiteCardModel {
        override fun setText(text: String): WhiteCardModel {
            this.text = text
            return this
        }
    }

    private class MemoryBlackCardModel(override val id: String, override var text: String, override val answerFields: Int, override val cardpackId: String) : BlackCardModel {
        override fun setText(text: String): BlackCardModel {
            this.text = text
            return this
        }
    }
}