import com.fasterxml.jackson.databind.ObjectMapper
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import database.memorymodel.MemoryCardCollection
import database.memorymodel.MemoryUserCollection
import database.mongomodel.MongoCardCollection
import database.mongomodel.MongoUserCollection
import org.bson.types.ObjectId
import route.card.JsonBlackCard
import route.card.JsonWhiteCard
import route.card.model.CardCollection
import route.user.model.UserCollection
import java.util.Date
import kotlin.test.assertTrue

class CardCollectionTest {
    private data class CollectionGroup(
        val userCollection: UserCollection,
        val cardCollection: CardCollection
    )

    private var collections = listOf<CollectionGroup>()

    private fun generateFakeId(): String {
        return ObjectId().toHexString()
    }

    @BeforeEach
    fun reset() {
        resetTestMongo()

        val memUserCollection = MemoryUserCollection()
        val memCardCollection = MemoryCardCollection(memUserCollection)

        val mongoUserCollection = MongoUserCollection(getTestMongoCollection("users"))
        val mongoCardCollection = MongoCardCollection(getTestMongoCollection("cardpacks"), mongoUserCollection)
        collections = listOf(CollectionGroup(memUserCollection, memCardCollection), CollectionGroup(mongoUserCollection, mongoCardCollection))
    }

    @TestFactory
    fun createCardpackForExistingUser(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            assertCardpackEquals(collections.cardCollection.getCardpack(cardpack.id), cardpack)
        } }
    }

    @TestFactory
    fun createCardpackForNonExistingUser(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeUserId = generateFakeId()
            val e = assertThrows(Exception::class.java) { collections.cardCollection.createCardpack("cardpack_name", fakeUserId) }
            assertEquals("User does not exist with id: $fakeUserId", e.message)
        } }
    }

    @TestFactory
    fun createWhiteCardForExistingCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            assertEquals(0, collections.cardCollection.getCardpack(cardpack.id).whiteCards.size)
            val card = collections.cardCollection.createWhiteCard(JsonWhiteCard("card_text"), cardpack.id)
            assertEquals(1, collections.cardCollection.getCardpack(cardpack.id).whiteCards.size)
            assertEquals("card_text", collections.cardCollection.getCardpack(cardpack.id).whiteCards[0].text)
        } }
    }

    @TestFactory
    fun createWhiteCardForNonExistingCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeCardpackId = generateFakeId()
            val e = assertThrows(Exception::class.java) { collections.cardCollection.createWhiteCard(JsonWhiteCard("card_text"), fakeCardpackId) }
            assertEquals("Cardpack does not exist with id: $fakeCardpackId", e.message)
        } }
    }

    @TestFactory
    fun createBlackCardForExistingCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            assertEquals(0, collections.cardCollection.getCardpack(cardpack.id).blackCards.size)
            val card = collections.cardCollection.createBlackCard(JsonBlackCard("card_text", 4), cardpack.id)
            assertEquals(1, collections.cardCollection.getCardpack(cardpack.id).blackCards.size)
            assertEquals("card_text", collections.cardCollection.getCardpack(cardpack.id).blackCards[0].text)
        } }
    }

    @TestFactory
    fun createBlackCardForNonExistingCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeCardpackId = generateFakeId()
            val e = assertThrows(Exception::class.java) { collections.cardCollection.createBlackCard(JsonBlackCard("card_text", 4), fakeCardpackId) }
            assertEquals("Cardpack does not exist with id: $fakeCardpackId", e.message)
        } }
    }

    @TestFactory
    fun createWhiteCardsForExistingCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            var cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            assertEquals(0, cardpack.whiteCards.size)
            assertEquals(0, collections.cardCollection.getCardpack(cardpack.id).whiteCards.size)
            val cards = collections.cardCollection.createWhiteCards(listOf(JsonWhiteCard("card_0"), JsonWhiteCard("card_1"), JsonWhiteCard("card_2")), cardpack.id)

            cardpack = collections.cardCollection.getCardpack(cardpack.id)

            assertEquals(3, cards.size)
            assertEquals("card_0", cards[0].text)
            assertEquals("card_1", cards[1].text)
            assertEquals("card_2", cards[2].text)
            assertEquals(cardpack.id, cards[0].cardpackId)
            assertEquals(cardpack.id, cards[1].cardpackId)
            assertEquals(cardpack.id, cards[2].cardpackId)

            assertEquals(3, cardpack.whiteCards.size)
            assertEquals("card_0", cardpack.whiteCards[0].text)
            assertEquals("card_1", cardpack.whiteCards[1].text)
            assertEquals("card_2", cardpack.whiteCards[2].text)
        } }
    }

    @TestFactory
    fun createBlackCardsForExistingCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            var cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            assertEquals(0, cardpack.blackCards.size)
            assertEquals(0, collections.cardCollection.getCardpack(cardpack.id).blackCards.size)
            val cards = collections.cardCollection.createBlackCards(listOf(JsonBlackCard("card_0", 1), JsonBlackCard("card_1", 2), JsonBlackCard("card_2", 3)), cardpack.id)

            cardpack = collections.cardCollection.getCardpack(cardpack.id)

            assertEquals(3, cards.size)
            assertEquals("card_0", cards[0].text)
            assertEquals("card_1", cards[1].text)
            assertEquals("card_2", cards[2].text)
            assertEquals(cardpack.id, cards[0].cardpackId)
            assertEquals(cardpack.id, cards[1].cardpackId)
            assertEquals(cardpack.id, cards[2].cardpackId)

            assertEquals(3, cardpack.blackCards.size)
            assertEquals("card_0", cardpack.blackCards[0].text)
            assertEquals("card_1", cardpack.blackCards[1].text)
            assertEquals("card_2", cardpack.blackCards[2].text)
        } }
    }

    @TestFactory
    fun createWhiteCardsForNonExistingCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeCardpackId = generateFakeId()
            val e = assertThrows(Exception::class.java) { collections.cardCollection.createWhiteCards(listOf(JsonWhiteCard("card_0"), JsonWhiteCard("card_1"), JsonWhiteCard("card_2")), fakeCardpackId) }
            assertEquals("Cardpack does not exist with id: $fakeCardpackId", e.message)
        } }
    }

    @TestFactory
    fun createBlackCardsForNonExistingCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeCardpackId = generateFakeId()
            val e = assertThrows(Exception::class.java) { collections.cardCollection.createBlackCards(listOf(JsonBlackCard("card_0", 1), JsonBlackCard("card_1", 1), JsonBlackCard("card_2", 1)), fakeCardpackId) }
            assertEquals("Cardpack does not exist with id: $fakeCardpackId", e.message)
        } }
    }

    @TestFactory
    fun deleteExistingCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            assertEquals(1, collections.cardCollection.getCardpacks(user.id).size)
            collections.cardCollection.deleteCardpack(cardpack.id)
            assertEquals(0, collections.cardCollection.getCardpacks(user.id).size)
        } }
    }

    @TestFactory
    fun deleteNonExistingCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeCardpackId = generateFakeId()
            var e = assertThrows(Exception::class.java) { collections.cardCollection.deleteCardpack(fakeCardpackId) }
            assertEquals("Cardpack does not exist with id: $fakeCardpackId", e.message)

            e = assertThrows(Exception::class.java) { collections.cardCollection.deleteCardpack("5b872f9c15b19b000175857f") }
            assertEquals("Cardpack does not exist with id: 5b872f9c15b19b000175857f", e.message)
        } }
    }

//    @TestFactory
//    fun removeExistingWhiteCardsWhenDeletingCardpack(): List<DynamicTest> {
//        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString(), {
//            val user = collections.userCollection.createUser("user", "1234", "google")
//            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
//            val card = collections.cardCollection.createWhiteCard(JsonWhiteCard("card_text", cardpack.id))
//            collections.cardCollection.deleteCardpack(cardpack.id)
//            val e = assertThrows(Exception::class.java) { collections.cardCollection.getCard(card.id) }
//            assertEquals("Card does not exist with id: ${card.id}", e.message)
//        })}
//    }
//
//    @TestFactory
//    fun removeExistingBlackCardsWhenDeletingCardpack(): List<DynamicTest> {
//        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString(), {
//            val user = collections.userCollection.createUser("user", "1234", "google")
//            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
//            val card = collections.cardCollection.createBlackCard(JsonBlackCard("card_text", 2, cardpack.id))
//            collections.cardCollection.deleteCardpack(cardpack.id)
//            val e = assertThrows(Exception::class.java) { collections.cardCollection.getCard(card.id) }
//            assertEquals("Card does not exist with id: ${card.id}", e.message)
//        })}
//    }

    @TestFactory
    fun deleteExistingWhiteCard(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val card = collections.cardCollection.createWhiteCard(JsonWhiteCard("card_text"), cardpack.id)
            collections.cardCollection.deleteWhiteCard(card.id, cardpack.id)
            assertEquals(0, collections.cardCollection.getCardpack(cardpack.id).whiteCards.size)
            assertEquals(0, cardpack.whiteCards.size)
            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteWhiteCard(card.id, cardpack.id) }
            assertEquals("Card does not exist with id: ${card.id}", e.message)
        } }
    }

    @TestFactory
    fun deleteNonExistingWhiteCard(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeCardId = generateFakeId()
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteWhiteCard(fakeCardId, cardpack.id) }
            assertEquals("Card does not exist with id: $fakeCardId", e.message)
        } }
    }

    @TestFactory
    fun deleteExistingBlackCard(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val card = collections.cardCollection.createBlackCard(JsonBlackCard("card_text", 1), cardpack.id)
            collections.cardCollection.deleteBlackCard(card.id, cardpack.id)
            assertEquals(0, collections.cardCollection.getCardpack(cardpack.id).blackCards.size)
            assertEquals(0, cardpack.blackCards.size)
            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteBlackCard(card.id, cardpack.id) }
            assertEquals("Card does not exist with id: ${card.id}", e.message)
        } }
    }

    @TestFactory
    fun deleteNonExistingBlackCard(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeCardId = generateFakeId()
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteBlackCard(fakeCardId, cardpack.id) }
            assertEquals("Card does not exist with id: $fakeCardId", e.message)
        } }
    }

    @TestFactory
    fun deleteExistingWhiteCards(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val cards = collections.cardCollection.createWhiteCards(listOf(JsonWhiteCard("card_0"), JsonWhiteCard("card_1"), JsonWhiteCard("card_2")), cardpack.id)
            collections.cardCollection.deleteWhiteCards(cards.map { card -> card.id }, cardpack.id)
            assertEquals(0, collections.cardCollection.getCardpack(cardpack.id).whiteCards.size)
            assertEquals(0, cardpack.whiteCards.size)
        } }
    }

    @TestFactory
    fun deleteExistingBlackCards(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val cards = collections.cardCollection.createBlackCards(listOf(JsonBlackCard("card_0", 1), JsonBlackCard("card_1", 1), JsonBlackCard("card_2", 1)), cardpack.id)
            collections.cardCollection.deleteBlackCards(cards.map { card -> card.id }, cardpack.id)
            assertEquals(0, collections.cardCollection.getCardpack(cardpack.id).blackCards.size)
            assertEquals(0, cardpack.blackCards.size)
        } }
    }

    @TestFactory
    fun deleteNonExistingWhiteCards(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteWhiteCards(listOf(generateFakeId(), generateFakeId()), cardpack.id) }
            assertEquals("One or more card ids is invalid", e.message)
        } }
    }

    @TestFactory
    fun deleteNonExistingBlackCards(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteBlackCards(listOf(generateFakeId(), generateFakeId()), cardpack.id) }
            assertEquals("One or more card ids is invalid", e.message)
        } }
    }

    @TestFactory
    fun createWhiteCardsAtomicity(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            // TODO - Implement
        } }
    }

    @TestFactory
    fun createBlackCardsAtomicity(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            // TODO - Implement
        } }
    }

    @TestFactory
    fun deleteExistingWhiteCardsAtomicity(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val cards = collections.cardCollection.createWhiteCards(listOf(JsonWhiteCard("card_0"), JsonWhiteCard("card_1"), JsonWhiteCard("card_2")), cardpack.id)
            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteWhiteCards(listOf(cards[0].id, generateFakeId(), cards[1].id, cards[2].id), cardpack.id) }
            assertEquals("One or more card ids is invalid", e.message)
            assertEquals(3, collections.cardCollection.getCardpack(cardpack.id).whiteCards.size)
        } }
    }

    @TestFactory
    fun deleteExistingBlackCardsAtomicity(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val cards = collections.cardCollection.createBlackCards(listOf(JsonBlackCard("card_0", 1), JsonBlackCard("card_1", 1), JsonBlackCard("card_2", 1)), cardpack.id)
            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteBlackCards(listOf(cards[0].id, generateFakeId(), cards[1].id, cards[2].id), cardpack.id) }
            assertEquals("One or more card ids is invalid", e.message)
            assertEquals(3, collections.cardCollection.getCardpack(cardpack.id).blackCards.size)
        } }
    }

    @TestFactory
    fun getExistingCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            var cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            collections.cardCollection.createWhiteCard(JsonWhiteCard("white_card_text"), cardpack.id)
            collections.cardCollection.createBlackCard(JsonBlackCard("black_card_text", 1), cardpack.id)
            cardpack = collections.cardCollection.getCardpack(cardpack.id)
            assertEquals(1, cardpack.whiteCards.size)
            assertEquals(1, cardpack.blackCards.size)
            assertEquals("white_card_text", cardpack.whiteCards[0].text)
            assertEquals("black_card_text", cardpack.blackCards[0].text)
            assertEquals(cardpack.id, cardpack.id)
        } }
    }

    @TestFactory
    fun getNonExistingCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeCardpackId = generateFakeId()
            val e = assertThrows(Exception::class.java) { collections.cardCollection.getCardpack(fakeCardpackId) }
            assertEquals("Cardpack does not exist with id: $fakeCardpackId", e.message)
        } }
    }

    @TestFactory
    fun getExistingCardpacks(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val userOne = collections.userCollection.createUser("user", "1234", "google")
            val userTwo = collections.userCollection.createUser("user", "4321", "google")
            val cardpackOne = collections.cardCollection.createCardpack("cardpack_name", userOne.id)
            val cardpackTwo = collections.cardCollection.createCardpack("cardpack_name", userTwo.id)
            val cardpackThree = collections.cardCollection.createCardpack("cardpack_name", userOne.id)
            val cardpackFour = collections.cardCollection.createCardpack("cardpack_name", userTwo.id)
            collections.cardCollection.createWhiteCard(JsonWhiteCard("white_card_text"), cardpackOne.id)
            collections.cardCollection.createBlackCard(JsonBlackCard("black_card_text", 1), cardpackOne.id)

            val userOneCardpacks = collections.cardCollection.getCardpacks(userOne.id)
            assertEquals(2, userOneCardpacks.size)
            assertEquals(cardpackOne.id, userOneCardpacks[0].id)
            assertEquals(cardpackThree.id, userOneCardpacks[1].id)
            assertEquals(1, userOneCardpacks[0].whiteCards.size)
            assertEquals(1, userOneCardpacks[0].blackCards.size)
            assertEquals("white_card_text", userOneCardpacks[0].whiteCards[0].text)
            assertEquals("black_card_text", userOneCardpacks[0].blackCards[0].text)

            val userTwoCardpacks = collections.cardCollection.getCardpacks(userTwo.id)
            assertEquals(cardpackTwo.id, userTwoCardpacks[0].id)
            assertEquals(cardpackFour.id, userTwoCardpacks[1].id)
        } }
    }

    @TestFactory
    fun getNonExistingCardpacks(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeUserId = generateFakeId()
            val e = assertThrows(Exception::class.java) { collections.cardCollection.getCardpacks(fakeUserId) }
            assertEquals("User does not exist with id: $fakeUserId", e.message)
        } }
    }

//    TODO - Get these tests working
//    @TestFactory
//    fun getExistingCard(): List<DynamicTest> {
//        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString(), {
//            val user = collections.userCollection.createUser("user", "1234", "google")
//            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
//            val card = collections.cardCollection.createCard("card_text", cardpack.id)
//            assert(cardEquals(card, collections.cardCollection.getCard(card.id)))
//        })}
//    }
//
//    @TestFactory
//    fun getNonExistingCard(): List<DynamicTest> {
//        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString(), {
//            val e = assertThrows(Exception::class.java) { collections.cardCollection.getCard("fake_card_id") }
//            assertEquals("Card does not exist with id: fake_card_id", e.message)
//        })}
//    }

    @TestFactory
    fun setCardpackName(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            cardpack.setName("new_cardpack_name")
            assertEquals("new_cardpack_name", cardpack.name)
            assertEquals("new_cardpack_name", collections.cardCollection.getCardpack(cardpack.id).name)
        } }
    }

    @TestFactory
    fun setWhiteCardText(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val card = collections.cardCollection.createWhiteCard(JsonWhiteCard("card_text"), cardpack.id)
            card.setText("new_card_text")
            assertEquals("new_card_text", card.text)
            assertEquals("new_card_text", collections.cardCollection.getCardpack(cardpack.id).whiteCards[0].text)
        } }
    }

    @TestFactory
    fun setBlackCardText(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val card = collections.cardCollection.createBlackCard(JsonBlackCard("card_text", 4), cardpack.id)
            card.setText("new_card_text")
            assertEquals("new_card_text", card.text)
            assertEquals("new_card_text", collections.cardCollection.getCardpack(cardpack.id).blackCards[0].text)
        } }
    }

    @TestFactory
    fun settingWhiteCardTextOnlyChangesSingleCard(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val cards = collections.cardCollection.createWhiteCards(listOf(JsonWhiteCard("card_text"), JsonWhiteCard("card_text")), cardpack.id)
            cards[0].setText("new_card_text")

            assertEquals("new_card_text", cards[0].text)
            assertEquals("new_card_text", collections.cardCollection.getCardpack(cardpack.id).whiteCards[0].text)

            assertEquals("card_text", cards[1].text)
            assertEquals("card_text", collections.cardCollection.getCardpack(cardpack.id).whiteCards[1].text)
        } }
    }

    @TestFactory
    fun settingBlackCardTextOnlyChangesSingleCard(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val cards = collections.cardCollection.createBlackCards(listOf(JsonBlackCard("card_text", 1), JsonBlackCard("card_text", 1)), cardpack.id)
            cards[0].setText("new_card_text")

            assertEquals("new_card_text", cards[0].text)
            assertEquals("new_card_text", collections.cardCollection.getCardpack(cardpack.id).blackCards[0].text)

            assertEquals("card_text", cards[1].text)
            assertEquals("card_text", collections.cardCollection.getCardpack(cardpack.id).blackCards[1].text)
        } }
    }

    @TestFactory
    fun cardpackContainsCorrectTimestamp(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")

            val threshold = 1100
            val expectedCreationTime = Date().time

            val cardpack = collections.cardCollection.createCardpack("cardpack_name", user.id)
            val actualCreationTime = cardpack.createdAt.time

            assert(Math.abs(expectedCreationTime - actualCreationTime) < threshold)

            assertEquals(actualCreationTime, cardpack.createdAt.time)
            assertEquals(actualCreationTime, collections.cardCollection.getCardpack(cardpack.id).createdAt.time)
            Thread.sleep(500)
            assertEquals(actualCreationTime, cardpack.createdAt.time)
            assertEquals(actualCreationTime, collections.cardCollection.getCardpack(cardpack.id).createdAt.time)
        } }
    }

    @TestFactory
    fun convertCardpackToJson(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")

            val cardpack = collections.cardCollection.createCardpack("cardpack_one", user.id)
            collections.cardCollection.createWhiteCard(JsonWhiteCard("card_one"), cardpack.id)
            collections.cardCollection.createBlackCard(JsonBlackCard("card_two", 1), cardpack.id)

            // TODO - Replace with assertThrows
            try {
                ObjectMapper().writeValueAsString(collections.cardCollection.getCardpack(cardpack.id))
            } catch (e: Exception) {
                throw Error("Expected not to throw!", e)
            }
        } }
    }

    @TestFactory
    fun cannotFavoriteCardpackThatYouAlreadyFavorited(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val userOne = collections.userCollection.createUser("user", "1234", "google")
            val userTwo = collections.userCollection.createUser("user", "4321", "google")

            val cardpack = collections.cardCollection.createCardpack("cardpack_one", userOne.id)
            collections.cardCollection.favoriteCardpack(userTwo.id, cardpack.id)
            val e = assertThrows(Exception::class.java) { collections.cardCollection.favoriteCardpack(userTwo.id, cardpack.id) }
            assertEquals("Cardpack is already in your favorites", e.message)
        } }
    }

    @TestFactory
    fun cannotUnfavoriteCardpackThatYouHaveNotFavorited(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val userOne = collections.userCollection.createUser("user", "1234", "google")
            val userTwo = collections.userCollection.createUser("user", "4321", "google")

            val cardpack = collections.cardCollection.createCardpack("cardpack_one", userOne.id)
            val e = assertThrows(Exception::class.java) { collections.cardCollection.unfavoriteCardpack(userTwo.id, cardpack.id) }
            assertEquals("Cardpack is not in your favorites", e.message)
        } }
    }

    @TestFactory
    fun cannotFavoriteYourOwnCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")

            val cardpack = collections.cardCollection.createCardpack("cardpack_one", user.id)
            val e = assertThrows(Exception::class.java) { collections.cardCollection.favoriteCardpack(user.id, cardpack.id) }
            assertEquals("Cannot favorite your own cardpack", e.message)
        } }
    }

    @TestFactory
    fun canRetrieveFavoritedCardpacks(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val userOne = collections.userCollection.createUser("user", "1234", "google")
            val userTwo = collections.userCollection.createUser("user", "4321", "google")

            val cardpack = collections.cardCollection.createCardpack("cardpack_one", userOne.id)

            assertEquals(0, collections.cardCollection.getFavoritedCardpacks(userTwo.id).size)
            collections.cardCollection.favoriteCardpack(userTwo.id, cardpack.id)
            val favoritedPacks = collections.cardCollection.getFavoritedCardpacks(userTwo.id)
            assertEquals(1, favoritedPacks.size)
            assertEquals(cardpack.id, favoritedPacks[0].id)
        } }
    }

    @TestFactory
    fun cardpackIsAutomaticallyRemovedFromFavoritesWhenDeleted(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val userOne = collections.userCollection.createUser("user", "1234", "google")
            val userTwo = collections.userCollection.createUser("user", "4321", "google")

            val cardpack = collections.cardCollection.createCardpack("cardpack_one", userOne.id)

            collections.cardCollection.favoriteCardpack(userTwo.id, cardpack.id)
            collections.cardCollection.deleteCardpack(cardpack.id)
            assertEquals(0, collections.cardCollection.getFavoritedCardpacks(userTwo.id).size)
        } }
    }

    @TestFactory
    fun canUnfavoriteCardpack(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val userOne = collections.userCollection.createUser("user", "1234", "google")
            val userTwo = collections.userCollection.createUser("user", "4321", "google")

            val cardpack = collections.cardCollection.createCardpack("cardpack_one", userOne.id)

            collections.cardCollection.favoriteCardpack(userTwo.id, cardpack.id)
            collections.cardCollection.unfavoriteCardpack(userTwo.id, cardpack.id)
            assertEquals(0, collections.cardCollection.getFavoritedCardpacks(userTwo.id).size)
        } }
    }

    @TestFactory
    fun cannotFavoriteCardpackThatDoesNotExist(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeCardpackId = generateFakeId()
            val user = collections.userCollection.createUser("user", "1234", "google")

            val e = assertThrows(Exception::class.java) { collections.cardCollection.favoriteCardpack(user.id, fakeCardpackId) }
            assertEquals("Cardpack does not exist with id: $fakeCardpackId", e.message)
            assertEquals(0, collections.cardCollection.getFavoritedCardpacks(user.id).size)
        } }
    }

    @TestFactory
    fun cannotFavoriteAsUserThatDoesNotExist(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeUserId = generateFakeId()
            val user = collections.userCollection.createUser("user", "1234", "google")

            val cardpack = collections.cardCollection.createCardpack("cardpack_one", user.id)
            val e = assertThrows(Exception::class.java) { collections.cardCollection.favoriteCardpack(fakeUserId, cardpack.id) }
            assertEquals("User does not exist with id: $fakeUserId", e.message)
        } }
    }

    @TestFactory
    fun subscribingToCardpackTwiceDoesNotAffectLikeCount(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val userOne = collections.userCollection.createUser("userOne", "1234", "google")
            val userTwo = collections.userCollection.createUser("userTwo", "4321", "google")

            var cardpack = collections.cardCollection.createCardpack("cardpack_one", userOne.id)
            collections.cardCollection.favoriteCardpack(userTwo.id, cardpack.id)
            assertThrows(Exception::class.java) { collections.cardCollection.favoriteCardpack(userTwo.id, cardpack.id) }
            cardpack = collections.cardCollection.getCardpack(cardpack.id)

            assertEquals(1, cardpack.likeCount)
        } }
    }

    @TestFactory
    fun newCardpackHasNoLikes(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")

            val cardpack = collections.cardCollection.createCardpack("cardpack_one", user.id)

            assertEquals(0, cardpack.likeCount)
        } }
    }

    @TestFactory
    fun keepsTrackOfCardpackLikes(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "0", "google")
            val userOne = collections.userCollection.createUser("user", "1", "google")
            val userTwo = collections.userCollection.createUser("user", "2", "google")
            val userThree = collections.userCollection.createUser("user", "3", "google")
            val userFour = collections.userCollection.createUser("user", "4", "google")

            val cardpack = collections.cardCollection.createCardpack("cardpack_one", user.id)

            collections.cardCollection.favoriteCardpack(userOne.id, cardpack.id)
            collections.cardCollection.favoriteCardpack(userTwo.id, cardpack.id)

            assertEquals(2, collections.cardCollection.getCardpack(cardpack.id).likeCount)

            collections.cardCollection.favoriteCardpack(userThree.id, cardpack.id)
            collections.cardCollection.favoriteCardpack(userFour.id, cardpack.id)

            assertEquals(4, collections.cardCollection.getCardpack(cardpack.id).likeCount)

            collections.cardCollection.unfavoriteCardpack(userOne.id, cardpack.id)
            collections.cardCollection.unfavoriteCardpack(userTwo.id, cardpack.id)

            assertEquals(2, collections.cardCollection.getCardpack(cardpack.id).likeCount)

            collections.cardCollection.unfavoriteCardpack(userThree.id, cardpack.id)
            collections.cardCollection.unfavoriteCardpack(userFour.id, cardpack.id)

            assertEquals(0, collections.cardCollection.getCardpack(cardpack.id).likeCount)
        } }
    }

    @TestFactory
    fun userCardpacksContainCorrectCards(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "0", "google")

            val cardpackOne = collections.cardCollection.createCardpack("cardpackOne", user.id)
            val cardpackTwo = collections.cardCollection.createCardpack("cardpackTwo", user.id)

            collections.cardCollection.createWhiteCard(JsonWhiteCard("whitecardOne"), cardpackOne.id)
            collections.cardCollection.createBlackCard(JsonBlackCard("blackcardOne", 1), cardpackOne.id)

            collections.cardCollection.createWhiteCard(JsonWhiteCard("whitecardTwo"), cardpackTwo.id)
            collections.cardCollection.createBlackCard(JsonBlackCard("blackcardTwo", 1), cardpackTwo.id)

            val userCardpacks = collections.cardCollection.getCardpacks(user.id)

            assertEquals(2, userCardpacks.size)
            assertEquals(1, userCardpacks[0].whiteCards.size)
            assertEquals(1, userCardpacks[0].blackCards.size)
            assertEquals(1, userCardpacks[1].whiteCards.size)
            assertEquals(1, userCardpacks[1].blackCards.size)
            assertEquals("whitecardOne", userCardpacks[0].whiteCards[0].text)
            assertEquals("blackcardOne", userCardpacks[0].blackCards[0].text)
            assertEquals("whitecardTwo", userCardpacks[1].whiteCards[0].text)
            assertEquals("blackcardTwo", userCardpacks[1].blackCards[0].text)
        } }
    }

    @TestFactory
    fun correctlyReturnsWhetherCardpackIsFavorited(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val userOne = collections.userCollection.createUser("userOne", "0", "google")
            val userTwo = collections.userCollection.createUser("userTwo", "1", "google")

            val cardpack = collections.cardCollection.createCardpack("cardpackOne", userOne.id)

            assertEquals(false, collections.cardCollection.cardpackIsFavoritedByUser(userTwo.id, cardpack.id))

            collections.cardCollection.favoriteCardpack(userTwo.id, cardpack.id)

            assertEquals(true, collections.cardCollection.cardpackIsFavoritedByUser(userTwo.id, cardpack.id))

            collections.cardCollection.unfavoriteCardpack(userTwo.id, cardpack.id)

            assertEquals(false, collections.cardCollection.cardpackIsFavoritedByUser(userTwo.id, cardpack.id))
        } }
    }

    @TestFactory
    fun throwsErrorWhenCheckingIfCardpackIsFavoritedUsingInvalidCardpackId(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "0", "google")
            val e = assertThrows(Exception::class.java) { collections.cardCollection.cardpackIsFavoritedByUser(user.id, "507f191e810c19729de860ea") }
            assertEquals("Cardpack does not exist with id: 507f191e810c19729de860ea", e.message)
        } }
    }

    @TestFactory
    fun cannotDeleteWhiteCardWithFakeCardpackId(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeCardpackId = generateFakeId()
            val fakeCardId = generateFakeId()
            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteWhiteCard(fakeCardId, fakeCardpackId) }
            assertEquals("Cardpack does not exist with id: $fakeCardpackId", e.message)
        } }
    }

    @TestFactory
    fun cannotDeleteBlackCardWithFakeCardpackId(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeCardpackId = generateFakeId()
            val fakeCardId = generateFakeId()
            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteBlackCard(fakeCardId, fakeCardpackId) }
            assertEquals("Cardpack does not exist with id: $fakeCardpackId", e.message)
        } }
    }

    @TestFactory
    fun cannotDeleteWhiteCardsWithFakeCardpackId(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeCardpackId = generateFakeId()
            val fakeCardId = generateFakeId()
            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteWhiteCards(listOf(fakeCardId), fakeCardpackId) }
            assertEquals("Cardpack does not exist with id: $fakeCardpackId", e.message)
        } }
    }

    @TestFactory
    fun cannotDeleteBlackCardsWithFakeCardpackId(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val fakeCardpackId = generateFakeId()
            val fakeCardId = generateFakeId()
            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteBlackCards(listOf(fakeCardId), fakeCardpackId) }
            assertEquals("Cardpack does not exist with id: $fakeCardpackId", e.message)
        } }
    }

    @TestFactory
    fun cannotDeleteWhiteCardsFromTwoDifferentCardpacks(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpackOne = collections.cardCollection.createCardpack("cardpackOne", user.id)
            val cardpackTwo = collections.cardCollection.createCardpack("cardpackTwo", user.id)

            val cardpackOneCards = collections.cardCollection.createWhiteCards(listOf(JsonWhiteCard("card"), JsonWhiteCard("card")), cardpackOne.id)
            val cardpackTwoCards = collections.cardCollection.createWhiteCards(listOf(JsonWhiteCard("card"), JsonWhiteCard("card")), cardpackTwo.id)

            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteWhiteCards(listOf(cardpackOneCards[0].id, cardpackTwoCards[0].id), cardpackOne.id) }
            assertEquals("One or more card ids is invalid", e.message)
        } }
    }

    @TestFactory
    fun cannotDeleteBlackCardsFromTwoDifferentCardpacks(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val user = collections.userCollection.createUser("user", "1234", "google")
            val cardpackOne = collections.cardCollection.createCardpack("cardpackOne", user.id)
            val cardpackTwo = collections.cardCollection.createCardpack("cardpackTwo", user.id)

            val cardpackOneCards = collections.cardCollection.createBlackCards(listOf(JsonBlackCard("card", 1), JsonBlackCard("card", 1)), cardpackOne.id)
            val cardpackTwoCards = collections.cardCollection.createBlackCards(listOf(JsonBlackCard("card", 1), JsonBlackCard("card", 1)), cardpackTwo.id)

            val e = assertThrows(Exception::class.java) { collections.cardCollection.deleteBlackCards(listOf(cardpackOneCards[0].id, cardpackTwoCards[0].id), cardpackOne.id) }
            assertEquals("One or more card ids is invalid", e.message)
        } }
    }

    @TestFactory
    fun iterateOverCardpacks(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val userOne = collections.userCollection.createUser("John Pinkerton", "1234", "google")
            val userTwo = collections.userCollection.createUser("John Pinkerton", "5678", "google")
            val cardpackIdSet = HashSet<String>()
            for (i in 1..1000) {
                val cardpackOne = collections.cardCollection.createCardpack("name$i", userOne.id)
                val cardpackTwo = collections.cardCollection.createCardpack("name$i", userTwo.id)
                cardpackIdSet.add(cardpackOne.id)
                cardpackIdSet.add(cardpackTwo.id)
            }
            val cardpackIds = HashSet<String>()
            collections.cardCollection.forEachCardpack { cardpackIds.add(it.id) }

            kotlin.test.assertEquals(2000, cardpackIds.size)
            cardpackIds.forEach { assertTrue(cardpackIdSet.contains(it)) }
        } }
    }

    @TestFactory
    fun filtersCardpacks(): List<DynamicTest> {
        return collections.map { collections -> DynamicTest.dynamicTest(collections.cardCollection::class.java.toString()) {
            val userOne = collections.userCollection.createUser("John Pinkerton", "1234", "google")
            val userTwo = collections.userCollection.createUser("John Pinkerton", "5678", "google")

            val prefilteredCardpackIds = HashSet<String>()
            for (i in 1..10) {
                val cardpackOne = collections.cardCollection.createCardpack("name$i", userOne.id)
                val cardpackTwo = collections.cardCollection.createCardpack("name$i", userTwo.id)
                prefilteredCardpackIds.add(cardpackOne.id)
                prefilteredCardpackIds.add(cardpackTwo.id)
                prefilteredCardpackIds.add("fake_cardpack_id_$i")
            }

            kotlin.test.assertEquals(30, prefilteredCardpackIds.size)
            val filteredCardpackIds = collections.cardCollection.filterUnusedCardpackIds(prefilteredCardpackIds.toList())
            kotlin.test.assertEquals(10, filteredCardpackIds.size) // Should have filtered out the 20 existing cardpacks
        } }
    }
}