import com.amazonaws.auth.AWSStaticCredentialsProvider
import com.amazonaws.auth.AnonymousAWSCredentials
import com.amazonaws.client.builder.AwsClientBuilder
import com.amazonaws.services.s3.AmazonS3ClientBuilder
import com.google.common.net.MediaType
import io.findify.s3mock.S3Mock
import objectstore.MemoryObjectCollection
import objectstore.ObjectCollection
import objectstore.ObjectNotFoundException
import objectstore.S3ObjectCollection
import org.junit.jupiter.api.AfterAll
import org.junit.jupiter.api.Assertions.assertThrows
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.DynamicTest
import org.junit.jupiter.api.TestFactory
import java.io.File
import kotlin.test.assertEquals

class ObjectCollectionTest {
    companion object {
        private val s3Mock = S3Mock.Builder().withPort(8001).withInMemoryBackend().build()
        private val endpoint = AwsClientBuilder.EndpointConfiguration("http://localhost:8001", "us-west-2")
        private var collections = listOf<ObjectCollection>()

        init {
            s3Mock.start()
        }

        @AfterAll
        fun teardown() {
            s3Mock.shutdown()
        }
    }

    @BeforeEach
    fun reset() {
        val s3Client = AmazonS3ClientBuilder
                .standard()
                .withPathStyleAccessEnabled(true)
                .withEndpointConfiguration(endpoint)
                .withCredentials(AWSStaticCredentialsProvider(AnonymousAWSCredentials()))
                .build()
        if (s3Client.doesBucketExistV2("testbucket")) {
            s3Client.deleteBucket("testbucket")
        }
        s3Client.createBucket("testbucket")
        collections = listOf(S3ObjectCollection(s3Client, "testbucket"), MemoryObjectCollection())
    }

    @TestFactory
    fun storeAndRetrieveObject(): List<DynamicTest> {
        return collections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            collection.putObject("objectKey", "Hello, world".toByteArray())
            val obj = collection.getObject("objectKey")
            assertEquals("Hello, world", String(obj.data))
        } }
    }

    @TestFactory
    fun storeAndRetrieveImageObject(): List<DynamicTest> {
        return collections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            val imageBytes = File("src/test/resources/test.png").readBytes()
            collection.putObject("objectKey", imageBytes)
            val obj = collection.getObject("objectKey")
            obj.data.forEachIndexed { index, byte ->
                assertEquals(imageBytes[index], byte)
            }
        } }
    }

    @TestFactory
    fun storeAndRetrieveDataType(): List<DynamicTest> {
        return collections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            collection.putObject("objectKey", "Hello, world".toByteArray(), MediaType.PNG)
            var obj = collection.getObject("objectKey")
            assertEquals(MediaType.PNG, obj.type)

            collection.putObject("objectKey", "Hello, world".toByteArray(), MediaType.MP4_VIDEO)
            obj = collection.getObject("objectKey")
            assertEquals(MediaType.MP4_VIDEO, obj.type)
        } }
    }

    @TestFactory
    fun defaultTypeIsOctetStream(): List<DynamicTest> {
        return collections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            collection.putObject("objectKey", "Hello, world".toByteArray())
            var obj = collection.getObject("objectKey")
            assertEquals(MediaType.OCTET_STREAM, obj.type)
        } }
    }

    @TestFactory
    fun updateExistingObject(): List<DynamicTest> {
        return collections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            collection.putObject("objectKey", "Hello, world".toByteArray())
            collection.putObject("objectKey", "Hi there, world".toByteArray())
            val obj = collection.getObject("objectKey")
            assertEquals("Hi there, world", String(obj.data))
        } }
    }

    @TestFactory
    fun cannotRetrieveObjectAfterDeletingIt(): List<DynamicTest> {
        return collections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            collection.putObject("objectKey", "Hello, world".toByteArray())
            collection.deleteObject("objectKey")
            val e = assertThrows(ObjectNotFoundException::class.java) { collection.getObject("objectKey") }
            assertEquals("Object does not exist with key: objectKey", e.message)
        } }
    }

    @TestFactory
    fun throwsExceptionWhenAttemptingToDeleteObjectThatDoesNotExist(): List<DynamicTest> {
        return collections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            val e = assertThrows(ObjectNotFoundException::class.java) { collection.deleteObject("objectKey") }
            assertEquals("Object does not exist with key: objectKey", e.message)
        } }
    }

    @TestFactory
    fun throwsExceptionWhenRetrievingNonExistentObject(): List<DynamicTest> {
        return collections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            val e = assertThrows(ObjectNotFoundException::class.java) { collection.getObject("fakeObject") }
            assertEquals("Object does not exist with key: fakeObject", e.message)
        } }
    }

    @TestFactory
    fun cannotUseInvalidObjectKey(): List<DynamicTest> {
        return collections.map { collection -> DynamicTest.dynamicTest(collection::class.java.toString()) {
            var e: Exception = assertThrows(IllegalArgumentException::class.java) { collection.putObject("!@#$%^&*(", "Hello".toByteArray()) }
            assertEquals("Invalid object key", e.message)
            e = assertThrows(IllegalArgumentException::class.java) { collection.putObject("//", "Hello".toByteArray()) }
            assertEquals("Invalid object key", e.message)

            e = assertThrows(IllegalArgumentException::class.java) { collection.getObject("!@#$%^&*(") }
            assertEquals("Invalid object key", e.message)
            e = assertThrows(IllegalArgumentException::class.java) { collection.getObject("//") }
            assertEquals("Invalid object key", e.message)

            e = assertThrows(IllegalArgumentException::class.java) { collection.deleteObject("!@#$%^&*(") }
            assertEquals("Invalid object key", e.message)
            e = assertThrows(IllegalArgumentException::class.java) { collection.deleteObject("//") }
            assertEquals("Invalid object key", e.message)
        } }
    }
}